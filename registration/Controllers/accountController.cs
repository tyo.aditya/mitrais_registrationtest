﻿using registration.Models;
using registration.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace registration.Controllers
{
    public class accountController : Controller
    {
        // GET: account
        public ActionResult Index()
        {
            return View();
        }

        //Return Register view
        public ActionResult Register()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult SaveRegisterDetails(RegisterVM registerDetails)
        {

            if (ModelState.IsValid)
            {
                using (var databaseContext = new RegistrationEntities())
                {
                    tblRegistration reg = new tblRegistration();
                    tblRegistration checkMobileNumber = databaseContext.tblRegistrations.Where(query => query.mobileNumber.Equals(registerDetails.MobileNumber)).SingleOrDefault();
                    tblRegistration checkEmail = databaseContext.tblRegistrations.Where(query => query.email.Equals(registerDetails.Email)).SingleOrDefault();
                    if (checkMobileNumber != null)
                    {
                        ViewBag.Message = "Mobile Number Already Exist";
                    }
                    else if (checkEmail != null)
                    {
                        ViewBag.Message = "Email Already Exist";
                    }
                    else
                    {
                        reg.mobileNumber = registerDetails.MobileNumber;
                        reg.firstName = registerDetails.FirstName;
                        reg.lastName = registerDetails.LastName;
                        reg.dateOfBirth = registerDetails.DateOfBirth;
                        reg.gender = registerDetails.Gender;
                        reg.email = registerDetails.Email;

                        databaseContext.tblRegistrations.Add(reg);
                        databaseContext.SaveChanges();
                        ViewBag.Message = "Congratulations you have been registered";
                        ViewBag.Data = "registered";
                    }
                    
                }
                return View("Register");
            }
            else
            {
                return View("Register", registerDetails);
            }
        }

        [HttpPost]
        public ActionResult Login()
        {
            return View();
        }



    }
}