USE [master]
GO
/****** Object:  Database [Registration]    Script Date: 1/17/2020 11:22:29 AM ******/
CREATE DATABASE [Registration]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Registration', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\Registration.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Registration_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\Registration_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Registration] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Registration].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Registration] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Registration] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Registration] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Registration] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Registration] SET ARITHABORT OFF 
GO
ALTER DATABASE [Registration] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Registration] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Registration] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Registration] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Registration] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Registration] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Registration] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Registration] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Registration] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Registration] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Registration] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Registration] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Registration] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Registration] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Registration] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Registration] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Registration] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Registration] SET RECOVERY FULL 
GO
ALTER DATABASE [Registration] SET  MULTI_USER 
GO
ALTER DATABASE [Registration] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Registration] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Registration] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Registration] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Registration] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'Registration', N'ON'
GO
ALTER DATABASE [Registration] SET QUERY_STORE = OFF
GO
USE [Registration]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
USE [Registration]
GO
/****** Object:  Table [dbo].[tblRegistration]    Script Date: 1/17/2020 11:22:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblRegistration](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[mobileNumber] [varchar](255) NULL,
	[firstName] [varchar](100) NULL,
	[lastName] [varchar](100) NULL,
	[dateOfBirth] [datetime] NOT NULL,
	[gender] [bit] NOT NULL,
	[email] [varchar](100) NULL,
 CONSTRAINT [PK_tblRegistration] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tblRegistration] ON 

INSERT [dbo].[tblRegistration] ([id], [mobileNumber], [firstName], [lastName], [dateOfBirth], [gender], [email]) VALUES (1, N'08562284000', N'tyo', N'aditya', CAST(N'2020-01-17T00:00:00.000' AS DateTime), 1, N'prasetyoadityaputra@gmail.com')
INSERT [dbo].[tblRegistration] ([id], [mobileNumber], [firstName], [lastName], [dateOfBirth], [gender], [email]) VALUES (2, N'08562284001', N'tyo', N'aditya', CAST(N'2020-01-17T00:00:00.000' AS DateTime), 0, N'prasetyoa@gmail.com')
INSERT [dbo].[tblRegistration] ([id], [mobileNumber], [firstName], [lastName], [dateOfBirth], [gender], [email]) VALUES (3, N'085622840', N'tyo', N'aditya', CAST(N'2020-01-09T00:00:00.000' AS DateTime), 0, N'aditya@gmail.com')
SET IDENTITY_INSERT [dbo].[tblRegistration] OFF
USE [master]
GO
ALTER DATABASE [Registration] SET  READ_WRITE 
GO
